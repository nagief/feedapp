<!doctype html>

<html class="no-js " lang="en"> 
<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
    <!-- Site Meta -->
    <title>Aplikasi Manajemen Kerja Karyawan</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

	<!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet"> 

	<!-- Custom & Default Styles -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/carousel.css">
    <link rel="stylesheet" href="style.css">

	<!-- Custom Form -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
<style type="text/css">
	body {
		color: #999;
		background: #f3f3f3;
		font-family: 'Roboto', sans-serif;
	}
    .form-control {
		border-color: #eee;
        min-height: 41px;
		box-shadow: none !important;
	}
    .form-control:focus {
		border-color: #5cd3b4;
	}
    .form-control, .btn {        
        border-radius: 3px;
    }
	.signup-form {
		width: 500px;
		margin: 0 auto;
		padding: 30px 0;
	}
    .signup-form h2 {
		color: #333;
        margin: 0 0 30px 0;
		display: inline-block;
		padding: 0 30px 10px 0;
		border-bottom: 3px solid #5cd3b4;
    }
    .signup-form form {
		color: #999;
		border-radius: 3px;
    	margin-bottom: 15px;
        background: #fff;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }
	.signup-form .form-group {
		margin-bottom: 20px;
	}
	.signup-form label {
		font-weight: normal;
		font-size: 13px;
	}
    .signup-form input[type="checkbox"] {
		margin-top: 2px;
	}
    .signup-form .btn {        
        font-size: 16px;
        font-weight: bold;
		background: #5cd3b4;
		border: none;
		margin-top: 20px;
		min-width: 140px;
    }
	.signup-form .btn:hover, .signup-form .btn:focus {
		background: #41cba9;
        outline: none !important;
	}
    .signup-form a {
		color: #5cd3b4;
		text-decoration: underline;
	}
	.signup-form a:hover {
		text-decoration: none;
	}
    .signup-form form a {
		color: #5cd3b4;
		text-decoration: none;
	}	
	.signup-form form a:hover {
		text-decoration: underline;
	}
</style>

</head>
<body class="left-menu">  
    

        <header class="vertical-header">
            <div class="vertical-header-wrapper">
                <nav class="nav-menu">
                    <div class="logo">
                        <img src="images/logo1.png" alt=""></a>
						<BR>BADAN PUSAT STATISTIK
                    </div><!-- end logo -->

                    <div class="margin-block"></div>

				<ul class="primary-menu">
                        <li class="child-menu"><a href="adminOutput.php">Tabel Data Kerja<i class="fa fa-angle-right"></i></a>
						</li>
                        <li class="child-menu"><a href="tambah.php">Tambah Data Kerja<i class="fa fa-angle-right"></i></a>
                        </li>
						<li class="child-menu"><a href="kelolaAdmin.php">Kelola Admin<i class="fa fa-angle-right"></i></a>
                        </li>
							<li class="child-menu"><a href="index.php">Log Out<i class="fa fa-angle-right"></i></a>
                        </li>
					
                         </ul>
					
					
                        <div class="menu-social">
                        <ul class="list-inline text-center">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div><!-- end menu -->
                </nav><!-- end nav-menu -->
				<br><br><br><br><br><br><br><br>
            </div><!-- end vertical-header-wrapper -->
        </header><!-- end header -->
    </div><!-- end menu-wrapper -->

    <div id="wrapper">

        <div id="home" class="video-section js-height-full">
            <div class="overlay"></div>
            <div class="home-text-wrapper relative container">
			
			<div class="signup-form">
    <form action="prosestambah.php" method="POST" class="form-horizontal">
		<div class="col-xs-8 col-xs-offset-4">
			<h2>Data Kerja</h2>
		</div>		
        <div class="form-group">
			<label class="control-label col-xs-4">Judul Tugas</label>
			<div class="col-xs-8">
                <input type="text" class="form-control" name="judul" required="required">
            </div>        	
        </div>
		<div class="form-group">
			<label class="control-label col-xs-4">Penanggung Jawab</label>
			<div class="col-xs-8">
                <select name="penanggung" class="form-control" >
				<option> -- Pilih -- </option>
				<option> Ekonomi Statistik </option>
				<option> Geo Statistik </option>
				<option> Sultan Statistik </option>
				<option> Divisi Kependudukan </option>
				<option> Direktori Statistik </option>
				</select>
            </div>        	
        </div>

		 <div class="form-group">
			<label class="control-label col-xs-4">Tanggal Rilis</label>
			<div class="col-xs-8">
                <input type="date" class="form-control" name="rilis" value="2018-07-22" 
				required="required">
            </div>        	
        </div>
		
		
		 <div class="form-group">
			<label class="control-label col-xs-4">Tanggal Deadline</label>
			<div class="col-xs-8">
                <input type="date" class="form-control" name="deadline" value="2018-07-22" 
				required="required">
            </div>        	
        </div>
		
		
	
	<div class="form-group">
			<label class="control-label col-xs-4">Status</label>
			<div class="col-xs-8">
                <select name="status" class="form-control">
				<option> -- Pilih -- </option>
				<option> Selesai </option>
				<option> Belum </option>
				</select>
            </div>        	
        </div>
	
	
		<div class="form-group">
			<div class="col-xs-8 col-xs-offset-4">
				<button type="submit" class="btn btn-primary btn-lg" name="simpan" vaue="Simpan">Simpan</button>
				<button type="reset" class="btn btn-primary btn-lg" name="simpan" vaue="Simpan">Reset</button>
			</div>  
		</div>		      
    </form>
	
</div>
			
                <div class="home-message">

				

                </div>
            </div>
        </div>

    <!-- jQuery Files -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/carousel.js"></script>
    <script src="js/parallax.js"></script>
    <script src="js/rotate.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/masonry.js"></script>
    <script src="js/masonry-4-col.js"></script>
    <!-- VIDEO BG PLUGINS -->
    <script src="videos/libs/swfobject.js"></script> 
    <script src="videos/libs/modernizr.video.js"></script> 
    <script src="videos/libs/video_background.js"></script> 
    <script>
        jQuery(document).ready(function($) {
            var Video_back = new video_background($("#home"), { 
                "position": "absolute", //Follow page scroll
                "z-index": "-1",        //Behind everything
                "loop": true,           //Loop when it reaches the end
                "autoplay": true,       //Autoplay at start
                "muted": true,          //Muted at start
                "mp4":"videos/video.mp4" ,     //Path to video mp4 format
                "video_ratio": 1.7778,              // width/height -> If none provided sizing of the video is set to adjust
                "fallback_image": "images/dummy.png",   //Fallback image path
                "priority": "html5"             //Priority for html5 (if set to flash and tested locally will give a flash security error)
            });
        });
    </script>

</body>
</html>