<!doctype html>

<html class="no-js " lang="en"> 
<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
    <!-- Site Meta -->
    <title>Aplikasi Survei Kepuasan</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

	<!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet"> 

	<!-- Custom & Default Styles -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/carousel.css">
    <link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="animasi.css">
	
	 <script src='https://code.responsivevoice.org/responsivevoice.js'></script>


</head>



    <div id="wrapper">
			<?php
				include 'koneksi.php';
			
			$data = mysqli_query($koneksi,"select * from kepuasan");
			$data2 = mysqli_query($koneksi,"select * from kepuasan WHERE id='2'");
			$data3 = mysqli_query($koneksi,"select * from kepuasan WHERE id='3'");
		($d = mysqli_fetch_array($data));
		($d2 = mysqli_fetch_array($data2));
		($d3 = mysqli_fetch_array($data3));
				?>	
				
        <div id="" style="background: url('images/background2.jpg');  background-size: cover;
     background-attachment: fixed;
     background-repeat: no-repeat;  " class="video-section js-height-full">
		
            <div class="overlay" ></div>
            <div class="home-text-wrapper relative container">
                <div class="home-message">
                   <div class="animated infinite zoomIn">
                    <p>Apakah anda puas dengan layanan kami?</p>
                    <div class="btn-wrapper"><br><br>
                        <div class="text-center">
                            <a href="edit.php?id=<?php echo $d['id'];?>" class="btn btn-default2">
							<img class="animated infinite rubberBand" style="border-radius:20%" src="images/sangatpuas1.jpg" width="250px">
							<br>SANGAT PUAS</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							
							<a href="edit.php?id=<?php echo $d2['id'];?>" class="btn btn-default2">
							<img class="animated infinite rubberBand" style="border-radius:20%" src="images/puas1.jpg" width="250px">
							<br>PUAS</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							
							<a href="edit.php?id=<?php echo $d3['id']; ?>" class="btn btn-default2">
							<img class="animated infinite rubberBand" style="border-radius:20%" src="images/tidakpuas1.jpg" width="250px"><br>
							TIDAK PUAS</a> 
                        </div>
                        </div>
                    </div><!-- end row -->
                </div>
            </div>
        </div>

    <!-- jQuery Files -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/carousel.js"></script>
    <script src="js/parallax.js"></script>
    <script src="js/rotate.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/masonry.js"></script>
    <script src="js/masonry-4-col.js"></script>
    <!-- VIDEO BG PLUGINS -->
    <script src="videos/libs/swfobject.js"></script> 
    <script src="videos/libs/modernizr.video.js"></script> 
    <script src="videos/libs/video_background.js"></script> 
    <script>
        jQuery(document).ready(function($) {
            var Video_back = new video_background($("#home"), { 
                "position": "absolute", //Follow page scroll
                "z-index": "-1",        //Behind everything
                "loop": true,           //Loop when it reaches the end
                "autoplay": true,       //Autoplay at start
                "muted": true,          //Muted at start
                "mp4":"videos/video.mp4" ,     //Path to video mp4 format
                "video_ratio": 1.7778,              // width/height -> If none provided sizing of the video is set to adjust
                "fallback_image": "images/dummy.png",   //Fallback image path
                "priority": "html5"             //Priority for html5 (if set to flash and tested locally will give a flash security error)
            });
        });
    </script>

	<script type="text/javascript">
  responsiveVoice.OnVoiceReady = function() {
   console.log("speech time?");
   responsiveVoice.speak(
  "Pilih salah satu ekspresi yang menggambarkan tingkat kepuasan anda!",
  "Indonesian Male",
  {
   pitch: 1, 
   rate: 1, 
   volume: 1
  }
 );
};
 </script>


</body>
</html>